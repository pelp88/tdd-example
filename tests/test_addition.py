from src.app import util


def test_add_two_positive_numbers():
    result = util.add(1, 1)
    assert result == 2


def test_add_two_negative_numbers():
    result = util.add(-1, -1)
    assert result == -2
