import pytest

from src.app import util


def test_always_pass():
    assert 42 == 42


def test_positive():
    assert util.inc(3) == 4


def test_negative():
    assert util.inc(-3) == -2


test_param_cases = [
    (3, 4),
    (-5, -4),
    (0, 1),
    (4.5, 5.5)
]


@pytest.mark.parametrize("x, expected", test_param_cases)
def test_increment_parametrized(x, expected):
    assert util.inc(x) == expected


def test_increment_non_numeric():
    with pytest.raises(TypeError) as e:
        util.inc('qwerty')
    assert 'the value should be int or float' in str(e)
